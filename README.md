## From the Author

I approached this project like I would approach a proof-of-concept work or an "innovation" project done during the 10% innovation time.
Application included here works well enough to demonstrate the concept, but it runs sub-optimally and isn't production-grade.
Some of shortcuts taken:

- Completely skipped async execution / multi-threading
    - This affects how quickly Mixer is able to go through deposit addresses and make transfers
- Completely skipped metrics and logging
- Test coverage is very limited
    - Mainly due to lots of side-effecting code and little business logic
- Mixer logic got a bit ugly
    - I refactored a part of the `List[Either[List[Either[]]]]` structure with the Either monad transformer, but it can be better
- Redis is used instead of a durable storage


## Architecture Overview

This application is made of the following moving parts:

1. User-facing online service (Client)
2. Offline service in charge off mixing coins (Mixer)
3. Data store through which data flows from the client to the mixer

This design was driven by the idea that there are two distinct parts: one that is interactive and face the user, and the other one which processes transactions at its own pace.

Redis was picked as it seemed the easiest to install, setup, and run. 
And Redis client library for Scala proved to work well.

### On-line service (Client)
 - Interactive user-facing service
 - Responsible for taking addresses from users and issuing deposit addresses
 - Saves mappings of deposit addresses to destination addresses into the database

### Off-line service (Mixer)
 - Designed to run as a background job (kicked off by cron or some other external service)
 - Goes through every deposit address and checks if balance is positive
    - Transfers balance to the house account
    - Splits balance into N parts randomly, where N is the number of destination addresses provided by the client
    - Transfers each part to a destination account   

### Data store
- Keeps mappings of deposit addresses issued to user and destination address provided by the users   

## How to run locally

### Prerequisites
 - Empty Redis running on default port (6379)
    - this project doesn't use key namespacing
 - Internet connection

### Steps
1. Launch client and submit a list of comma-separated destination addresses where jobcoins should be deposited
2. Transfer coins
3. Run mixer once
4. Check account balances 

### Client
- Inside client folder: `sbt run`
- Select `[1] Main`

### Mixer
- Inside mixer folder: `sbt run`
- Mixer will run once and terminate
- Will need to run it again after transferring more coins to the deposit address 