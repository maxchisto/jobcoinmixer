import javax.print.attribute.standard.Destination
import model.{Address, CoinCount}
import util.{AddressProcessor, DBClient, InsufficientFunds, JobcoinAPIClient}

class MixerClient(apiClient: JobcoinAPIClient, dbclient: DBClient) {

  val addrProc = new AddressProcessor(dbclient)

  def mixCoins(amount: CoinCount, source: Address, destAddresses: List[Address]): Either[Exception, Unit] = {
    for {
      depositAddress <- getDepositAddress(destAddresses)
      _ <- transferCoins(source, depositAddress, amount)
    } yield ()
  }

  def transferCoins(source: Address, destination: Address, amount: CoinCount): Either[Exception, Unit] = {
    for {
      _ <- checkBalance(source, amount)
      _ <- apiClient.postTransaction(amount, source, destination)
    } yield ()

  }

  def checkBalance(address: Address, amount: CoinCount): Either[Exception, Unit] =
    apiClient.getBalance(address).flatMap {
      balance => if (balance.value >= amount.value) {
        Right(())
      } else Left(InsufficientFunds)
    }


  def getDepositAddress(addresses: List[Address]): Either[Exception, Address] = {
    addrProc.issueHouseAddress(addresses)
  }

}
