import model.{Address, CoinCount}
import util.{Config, DBClient, JobcoinAPIClient}

object Playground extends App {

  val config = Config(
    redisUrl = "localhost",
    redisPort = 6379
  )

  val apiClient = new JobcoinAPIClient("jobcoin.gemini.com/dubbed-swaddling")
  val dbclient = new DBClient(config)
  val mixerClient = new MixerClient(apiClient, dbclient)

  val alice = Address("Alice")
  val bob = Address("Bob")

  val one = Address("1")
  val two = Address("2")

  val deposit = Address("ce2be95f-6cbb-4680-8e81-d01fc574f416")

  println(apiClient.postTransaction(CoinCount(2), alice, deposit))

//  println(apiClient.getBalance(alice))

//  println(mixerClient.getDepositAddress(List(one, two)))
//
//  println(dbclient.getAllByKey("2532f410-9299-4378-872c-cbd9dde60709"))
//  println(dbclient.getAllByKey("9665d6cf-c54a-4656-8e6c-b98061cc6bb0"))
//
//  println(dbclient.getAllKeys())
}
