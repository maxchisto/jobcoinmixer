package util

sealed trait JobcoinError extends Exception

case object InsufficientFunds extends JobcoinError

case class HttpPostError(url: String, params: List[(String, String)], reason: String) extends JobcoinError

case class DBClientError(reason: String) extends JobcoinError

case object CompletedException extends JobcoinError