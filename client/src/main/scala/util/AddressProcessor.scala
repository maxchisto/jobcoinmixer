package util

import model.Address

import scala.util.{Failure, Success, Try}

class AddressProcessor(dbclient: DBClient) {

  def issueHouseAddress(destinationAddress: List[Address]): Either[Exception, Address] = {
    val houseAddress = Address(java.util.UUID.randomUUID().toString)

    for {
      _ <- saveMapping(houseAddress, destinationAddress)
    } yield houseAddress
  }

  def saveMapping(houseAddress: Address, destinations: List[Address]): Either[Exception, Unit] = {
    Try {
      dbclient.setKey(houseAddress.value, destinations.map(_.value))
    } match {
      case Success(_) => Right(())
      case Failure(e) => Left(DBClientError(e.getCause.toString))
    }
  }

}
