package util

import model.Address

case class Config (
  redisUrl: String,
  redisPort: Int
)
