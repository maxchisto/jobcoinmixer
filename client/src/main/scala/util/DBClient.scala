package util

import com.redis.RedisClient

class DBClient(config: Config) {

  val redisClient = new RedisClient(config.redisUrl, config.redisPort)

  def getAllByKey(key: String): Option[List[Option[String]]] = {
    redisClient.lrange(key, 0, -1)

  }

  def setKey(key: String, values: List[String]) = {
    values.foreach( v => redisClient.rpush(key, v))
  }

  def getAllKeys() = {
    redisClient.keys()
  }

}
