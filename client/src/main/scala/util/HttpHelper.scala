package util

import java.util

import org.apache.commons._
import org.apache.http._
import org.apache.http.client._
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils

object HttpHelper {

  def post(url: String, params: List[(String, String)]): Either[HttpPostError, Unit] = {

    val client = HttpClients.createDefault()
    val post = new HttpPost(url)

    val nameValuePairs = new util.ArrayList[NameValuePair]()

    params.foreach { x =>
      nameValuePairs.add(new BasicNameValuePair(x._1, x._2))
    }

    post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
    val response = client.execute(post)

    val statusCode = response.getStatusLine.getStatusCode

    EntityUtils.consume(response.getEntity)

    if (statusCode == 200 ) {
      Right(())
    } else {
      println("http: " + response.getStatusLine.getReasonPhrase)
      Left(HttpPostError(url, params, response.getStatusLine.getReasonPhrase))
    }

  }

}



