import model.{Address, CoinCount}
import util.{CompletedException, Config, DBClient, JobcoinAPIClient}

import scala.io.StdIn

object Main extends App {

  val config = Config(
    redisUrl = "localhost",
    redisPort = 6379
  )

  val apiClient = new JobcoinAPIClient("jobcoin.gemini.com/dubbed-swaddling")
  val dbclient = new DBClient(config)
  val mixerClient = new MixerClient(apiClient, dbclient)

  val prompt: String = "Please enter a comma-separated list of new, unused Jobcoin addresses where your mixed Jobcoins will be sent."
  val helpText: String =
    """
      |Jobcoin Mixer
      |
      |Takes in at least one return address as parameters (where to send coins after mixing). Returns a deposit address to send coins to.
      |
      |Usage:
      |    run return_addresses...
    """.stripMargin


  try {
    while (true) {
      println(prompt)
      val line = StdIn.readLine()

      if (line == "quit") throw CompletedException

      val addresses = line.split(",")
      if (line == "") {
        println(s"You must specify empty addresses to mix into!\n$helpText")
      } else {

        val depositAddress = mixerClient.getDepositAddress(addresses.toList.map(Address)) match {
          case Left(e) => throw e
          case Right(addr) => addr
        }

        println(s"You may now send Jobcoins to address ${depositAddress.value}. They will be mixed and sent to your destination addresses.")
      }
    }
  } catch {
    case e =>
      println("Quitting...")
      println("Reason: " + e.toString)
  } finally {

  }







}
