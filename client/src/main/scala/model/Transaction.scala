package model


sealed trait Transaction

case class Deposit(
  amount: CoinCount,
  address: Address,
  timestamp: String
) extends Transaction

case class Transfer(
  amount: CoinCount,
  source: Address,
  destination: Address,
  timestamp: String
) extends Transaction
