import model.{Address, CoinCount}
import util.{Config, DBClient, DBClientError, JobcoinAPIClient}

import scala.annotation.tailrec
import cats.data.EitherT
import cats.implicits._

class Mixer(dbclient: DBClient, apiClient: JobcoinAPIClient, config: Config) {

  type Deposit = (Address, CoinCount)

  val random = scala.util.Random

  def mix = {
    getAllDeposits
      .filter_( _._2.value > 0)
      .map(processDeposit)
  }


  def getAllAddresses: Either[Exception, List[Address]] = {
    dbclient.getAllKeys match {
      case Some(xs) => Right(xs.map(x => Address(x.get)))
      case None => Left(DBClientError("Couldn't retrieve deposit addresses"))
    }
  }


  def getAllDeposits: EitherT[List, Exception, Deposit] = {
    getAllAddresses match {
      case Left(x) => EitherT.leftT(x)
      case Right(addrs) => EitherT(addrs.map(toDeposit))
    }
  }


  def toDeposit(addr: Address): Either[Exception, Deposit] = {
    val result  = for {
      balance <- apiClient.getBalance(addr)
    } yield (addr, balance)

    result
  }

  /** Can live outside of the class. */
  def splitCoins(coins: CoinCount, numParts: Int): List[CoinCount] = {

    @tailrec
    def split(coins: CoinCount, numParts: Int, acc: List[CoinCount]):  List[CoinCount] = {
      if (numParts == 1) {
        acc ++ List(coins)
      } else {
        val r = random.nextDouble
        val part = coins.value * r
        val remainder = CoinCount(coins.value - part)
        split(remainder, numParts - 1, acc ++ List(CoinCount(part)))
      }
    }

    split(coins, numParts, List())
  }

  def processDeposit(deposit: Deposit) = {
    val addr = deposit._1
    val coins = deposit._2

    for {
      // get destination addresses
      destAddrs <- dbclient.getAllByKey(addr.value).map(xs => xs.map(x => Address(x)))

      // transfer funds into the main "big house" account
      _ <- apiClient.postTransaction(coins, addr, config.bigHouseAcct)

      portions = splitCoins(coins, destAddrs.length)
      deposits = portions.zip(destAddrs)
      mixerResult = deposits.map {pair =>
        apiClient.postTransaction(pair._1, config.bigHouseAcct, pair._2)
      }
    } yield mixerResult

  }

}
