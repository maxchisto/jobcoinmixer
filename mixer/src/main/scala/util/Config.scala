package util

import model.Address

case class Config(
  bigHouseAcct: Address,
  redisUrl: String,
  redisPort: Int
)
