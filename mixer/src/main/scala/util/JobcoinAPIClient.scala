package util

import io.circe._
import io.circe.parser._
import model.{Address, CoinCount}

import scala.io.Source


class JobcoinAPIClient(baseUrl: String) {

  val apiPath = "https://" + baseUrl + "/api"


  def getBalance(address: Address): Either[Exception, CoinCount] = {
    val url = apiPath + "/addresses/" + address.value

    // little shortcut to avoid dealing with the http client
    val raw = Source.fromURL(url).mkString

    val result = parse(raw).getOrElse(Json.Null)

    val cursor = result.hcursor

    for {
      balance <- cursor.downField("balance").as[Double]
    } yield CoinCount(balance)

  }


  def postTransaction(amount: CoinCount, source: Address, destination: Address): Either[Exception, Unit] = {
    val target = apiPath + "/transactions"
    val params: List[(String, String)] = List(
      ("fromAddress", source.value),
      ("toAddress", destination.value),
      ("amount", amount.value.toString)
    )
    HttpHelper.post(target, params)
  }


}
