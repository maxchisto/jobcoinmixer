package util

import com.redis.RedisClient

class DBClient(config: Config) {

  val redisClient = new RedisClient(config.redisUrl, config.redisPort)

  /** Will return error if resulting list is empty */
  def getAllByKey(key: String): Either[Exception, List[String]] = {
    redisClient.lrange(key, 0, -1) match {
      case None => Left(DBClientError("No values found for key: " + key))
      case Some(xs) =>
        val values = xs.filter(_.isDefined).map(_.get)
        if (values.isEmpty) {
          Left(DBClientError("List of values was empty; key: " + key))
        }else {
          Right(values)
        }
    }

  }

  def setKey(key: String, values: List[String]) = {
    values.foreach( v => redisClient.rpush(key, v))
  }

  def getAllKeys = {
    redisClient.keys()
  }

}
