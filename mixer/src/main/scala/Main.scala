import model.Address
import util.{Config, DBClient, JobcoinAPIClient}

object Main extends App{

  val config = Config(
    bigHouseAcct = Address("house"),
    redisUrl = "localhost",
    redisPort = 6379
  )

  val apiClient = new JobcoinAPIClient("jobcoin.gemini.com/dubbed-swaddling")
  val dbclient = new DBClient(config)
  val mixerClient = new Mixer(dbclient, apiClient, config)


  println(mixerClient.mix)

}
