import model.{Address, CoinCount}
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest._
import util.{Config, DBClient, JobcoinAPIClient}

class TestMixer extends FlatSpec with Matchers {

  val config = Config(
    bigHouseAcct = Address("house"),
    redisUrl = "localhost",
    redisPort = 6379
  )

  val apiClient = new JobcoinAPIClient("localhost")
  val dbclient = new DBClient(config)
  val mixerClient = new Mixer(dbclient, apiClient, config)

  "Split coins" should "add up to total" in {
    val total = 5
    val parts = 3
    val split = mixerClient.splitCoins(CoinCount(total), parts)

    assert(total == split.map(_.value).sum)
  }

}
