import Dependencies._

ThisBuild / scalaVersion     := "2.13.1"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.jobcoin"
ThisBuild / organizationName := "jobcoin"

lazy val root = (project in file("."))
  .settings(
    name := "mixer",
    libraryDependencies += scalaTest % Test
  )

val circeVersion = "0.12.3"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.10"

libraryDependencies ++= Seq(
  "net.debasishg" %% "redisclient" % "3.20"
)

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
